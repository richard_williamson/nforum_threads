16/03/2015
Author: Richard Williamson
Format: MarkdownItex
I claim that given any monoidal datum $\mathbb{M} = (\mathcal{A},\mathcal{B},\otimes_{\mathbb{M}})$, there is a strict monoidal category $\mathcal{C}$ with the following universal property: given any strict monoidal category $\mathcal{D}$ for which $\mathbb{M}$ is a monoidal datum, there is a unique strictly monoidal functor 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/categorical_knot_theory/post_4-figure5.png' alt='' />

</div>

such that the diagrams 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/categorical_knot_theory/post_4-figure6.png' alt='' />

</div>

and 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/categorical_knot_theory/post_4-figure7.png' alt='' />

</div>

commute. I will refer to $\mathcal{C}$ as the 'free strict monoidal category on $\mathbb{M}$'. 

I claim that $\mathcal{C}$ moreover has a 2-universal property upgrading the above universal property, and that there will be analogous universal property and 2-universal property when $\mathcal{D}$ is a monoidal category which is not necessarily strict. One should indeed be able to see the construction of the free strict monoidal category on $\mathbb{M}$ as defining a 2-monad on the category of monoidal data (i.e. the category in which an object is a monoidal datum, and the arrows are defined in the appropriate way). I plan to come back to the details of these claims at a later point. The only one we will need for now is the existence of the free strict monoidal category on a monoidal datum with its 1-universal property: let us take it for granted.

There are various ways in which one could vary the notion of a monoidal datum. Most significantly, one could work with a category instead of a directed graph. However, in the examples that I have in mind, this category, let us denote it by $\mathcal{A}$, would be a free category on a directed graph, or a quotient of such a free category. To define the functor 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/categorical_knot_theory/post_4-figure8.png' alt='' />

</div>

which is part of the monoidal datum, one would then need to construct a functor out of a product of free categories. In the kind of categorical foundations in which I wish to work, this is in fact a tricky matter. 

At first it seems clear how to proceed: express a product of categories as a quotient of the free category on the product of the underlying directed graphs. However, to do this in general in the kind of foundations that I wish to work, one needs some kind of 'extensionality', allowing one to define a functor out of a category by specifying how this functor behaves on individual arrows (in other words, one needs to be able to 'glue together' one's category out of individual objects and arrows). I do not wish to adopt this kind of 'extensionality' as a foundational axiom. 

Instead of working with the 'native' notion of category, one could define a new, 'extensional', notion within the categorical foundations, for instance as the algebras for the free category monad. But I do not wish to take this route either. 

One could also construct, when working with very small categories as I will, the product of free categories by hand via colimits, in a way that will allow us to construct functors out of it. But this would be somewhat tedious.

Working with a directed graph instead of a category bypasses this difficulty entirely.
