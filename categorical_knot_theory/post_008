16/03/2015
Author: Richard Williamson
Format: MarkdownItex
In our pictorial notation, we see that two arrows are the same if the one can be obtained from the other by deleting pairs of parallel strands. For instance, the arrow pictured as 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/categorical_knot_theory/post_4-figure31.png' alt='' />

</div>

is the same as the arrow pictured as follows.

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/categorical_knot_theory/post_4-figure32.png' alt='' />

</div>

Here, as an aside, I would like to make a remark about my point of view on pictorial notation such as that I have described (we will see other examples in later posts). It is perfectly reasonable to describe a proof that two arrows (or other gadgets in a more complicated algebraic structure than a category) are equal by arguing pictorially. Bu t I will always understand such an argument as a recipe for carrying out a formalised proof without pictures.

This is not to say that a formal system could not be pictorial in nature: ultimately, a pictorial notation is made up of marks on a piece of paper, or bits of light on a screen, in just the same way as letters or other symbols which we conventionally use to communicate a mathematical proof, one human to another. But I have in mind a foundations expressed in letters and other symbols in the usual way, and a pictorial argument can only then be exactly that: something that we understand how to translate into a formal proof, but which is not a formal proof in itself. 

In particular, I completely disagree with the suggestion, made for instance in the third sentence of the remark before Lemma 2.1 in the paper [Traced monoidal categories](http://sci-prew.inf.ua/v119/3/S0305004100074338.pdf) of Joyal, Street, and Verity, that formalising pictorial notation in some topological way gives proofs in the pictorial notation a rigorous meaning _in themselves_. In fact, translating the pictorial argument into a rigorous argument within the topological formalisation is at least as difficult (I would say more difficult) than translating it directly into an argument in the category one is working in (i.e. where the notation comes from). And one then has to further appeal to significantly non-trivial results to pass from the topological formalisation to an argument in the category one is working in. I much prefer to  simply pass backwards and forwards between a category and the pictorial notation we have for it. 

This is not at all to say that topological formalisations of pictorial notations are not interesting. Quite the contrary, such formalisations can increase our understanding of the notation significantly, and the mathematics involved can be highly non-trivial. But one should not deceive oneself: results in such formalisations are 'meta-results' about the notation, and cannot be any more than this. These meta-results do not and cannot magically ensure that pictorial arguments are in themselves formal arguments.

My point of view is the same concerning commutative diagrams, and, more generally, pasting diagrams. One can formalise these notions, and prove interesting things which improve our understanding of them. But this does _not_ mean that a proof involving manipulating commutative diagrams, or more general pasting diagrams, such as the proofs in the [homotopy hypothesis](http://nforum.mathforge.org/discussion/6465/homotopy-hypothesis/#Item_0) thread that I am writing, is _in itself_ a formal proof. They must always be understood as recipes for constructing formal proofs. I do claim, of course, that this recipe is clear, and that the corresponding formal proofs are correct!

End of aside! Let us now get back to the construction of our category $\mathsf{Braids}$.
