16/03/2015
Author: Richard Williamson
Format: MarkdownItex
Let 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/categorical_knot_theory/post_4-figure33.png' alt='' />

</div>

be the morphism of directed graphs defined by the following.

1) We send $(1,1)$ to $2$.

2) We send $(1,2)$ to $3$.

3) We send $(2,1)$ to $3$.

4) We send $\big( id(1), id(1) \big)$ to $id(2)$.

5) We send $\big( id(1), \mathsf{OverCrossing} \big)$ to $id \otimes \mathsf{OverCrossing}$.

6) We send $\big( id(1), \mathsf{UnderCrossing} \big)$ to $id \otimes \mathsf{UnderCrossing}$.

7) We send $\big( \mathsf{OverCrossing}, id(1) \big)$ to $\mathsf{OverCrossing} \otimes id$.

8) We send $\big( \mathsf{UnderCrossing}, id(1) \big)$ to $\mathsf{UnderCrossing} \otimes id$.

Let $\mathsf{Braids}$ denote the free strict monoidal category on the monoidal datum $(\Gamma_{\mathsf{Braids}_{\leq 2}}, \mathsf{Braids}_{\leq 3}, \otimes)$. We shall think of the unit object of $\mathsf{Braids}$ as the 'braid on zero strings', an 'empty braid'. This category $\mathsf{Braids}$ is the 'category of braids' which we have been aiming to define. It is exactly the usual 'free braided monoidal category on an object' obtained by taking the coproduct of all the braid groups $B_{n}$ for $n \geq 0$. We shall not, however, make use of this point of view in our construction of the bracket polynomial, and the construction we have given of $\mathsf{Braids}$ is more suitable for our purposes than the 'coproduct construction' just described.

Here are a couple of other examples of free strict monoidal categories on monoidal data.

1) There is a unique monoidal datum $\mathbb{M} = (\Gamma, \mathcal{B}, \otimes)$ in which $\Gamma$ is the initial object of the category of directed graphs (i.e. the empty graph). The free strict monoidal category on this monoidal datum is the free strict monoidal category on $\mathcal{B}$ in the usual sense.

2) The category of 'cubes with connections', and other 'decorated categories of cubes' in which there are some generating arrows in dimension 2 or greater, can be described as the free strict monoidal category on a monoidal datum with unit. The notion of a 'monoidal datum with unit' is a variation on the notion of a monoidal datum which gives us the possibility of specifying the unit object in the 'free strict monoidal category' upon such a gadget (again I claim that such a free construction again exists, in the same various senses as the construction of the free strict monoidal category on a monoidal datum discussed earlier). I omit the details of this example here, as we will not need them.

We will see a further example of a free strict monoidal category on a monoidal datum in the next post in this thread, in which I will define a 'Temperley-Lieb' category. 

As a final remark, finding ways to understand canonical constructions of monoidal structures crops up in other places in my work. One important and very interesting one is the question of 'truncating' a monoidal structure: the case that I particularly have in mind is the 'truncation' of the monoidal structure of strict cubical $\infty$-groupoids or categories, coming from the monoidal structure on cubical sets, to a monoidal structure (the 'Gray tensor product') on strict cubical $n$-groupoids or categories, for any $n \geq 1$. I plan to eventually address this construction in the [thread on the homotopy hypothesis](http://nforum.mathforge.org/discussion/6465/homotopy-hypothesis/#Item_0).

[End of this post]
