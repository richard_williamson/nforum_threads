15/02/2015
Author: Richard Williamson
Format: MarkdownItex
Let 

<div style="text-align: center;">

<img src="https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_14-figure17.png" alt="" />

</div>

be the unit natural transformation of the adjunction between $\mathsf{Sd}$ and $\mathsf{Ex}$. 

Let $\mathsf{Ex} \cdot \phi$ denote the natural transformation

<div style="text-align: center;">

<img src="https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_14-figure18.png" alt="" />

</div>

obtained by whiskering the following diagram.

<div style="text-align: center;">

<img src="https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_14-figure19.png" alt="" />

</div> 

Let  

<div style="text-align: center;">

<img src="https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_14-figure20.png" alt="" />

</div>

be the composition of natural transformations $(\mathsf{Ex} \cdot \phi) \circ \eta$. In other words, the following triangle commutes. 

<div style="text-align: center;">

<img src="https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_14-figure21.png" alt="" />

</div>

This natural transformation $\iota$ is the one which we were aiming to define. Given a cubical set $X$, it for instance sends a 1-cube

<div style="text-align: center;">

<img src="https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_14-figure22.png" alt="" />

</div>

of $X$ to the 1-cube of $\mathsf{Ex}(X)$ pictured below, in which the right 1-cube is degenerate.

<div style="text-align: center;">

<img src="https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_14-figure23.png" alt="" />

</div>
