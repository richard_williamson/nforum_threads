16/02/2015
Author: Richard Williamson
Format: MarkdownItex
Let us denote the natural transformation

<div style="text-align: center;">

<img src="https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_16-figure18.png" alt="" />

</div>

by $\phi_{\leq 1}^{1}$, and denote the morphism of cubical sets 

<div style="text-align: center;">

<img src="https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_16-figure19.png" alt="" />

</div>

by $q^{1}$.
 
Inductively, I'll now define, for every integer $n \geq 1$, a natural transformation as follows.

<div style="text-align: center;">

<img src="https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_16-figure20.png" alt="" />

</div>

 Suppose that we have defined $\phi_{\leq 1}^{n}$. We then define $\phi_{\leq 1}^{n+1}$ in the following way.

1) To $I^{0}$, we associate the following identity morphism.

<div style="text-align: center;">

<img src="https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_16-figure21.png" alt="" />

</div>

2) To $I^{1}$, we associate the canonical morphism

<div style="text-align: center;">

<img src="https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_16-figure22.png" alt="" />

</div>

of cubical sets such that the following diagram in $\mathsf{Set}^{op}$ commutes. 

<div style="text-align: center;">

<img src="https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_16-figure23.png" alt="" />

</div>

It is straightforward to check that the three diagrams which must commute do indeed commute.

Recall that we can picture $\mathsf{Sd}_{\leq 1}^{3}(\square^{1})$ as follows

<div style="text-align: center;">

<img src="https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_16-figure24.png" alt="" />

</div>

and that we can picture $\mathsf{Sd}_{\leq 1}^{2}(\square^{1})$ as follows.

<div style="text-align: center;">

<img src="https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_16-figure25.png" alt="" />

</div>

The morphism 

<div style="text-align: center;">

<img src="https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_16-figure26.png" alt="" />

</div>

can be pictured as 'chopping off the last zig-zag' of $\mathsf{Sd}_{\leq 1}^{3}(\square^{1})$. Namely the first four 1-cubes, from the left, of $\mathsf{Sd}_{\leq 1}^{3}(\square^{1})$ are sent to the corresponding four non-degenerate 1-cubes of $\mathsf{Sd}_{\leq 1}^{2}(\square^{1})$; and the last two 1-cubes of $\mathsf{Sd}_{\leq 1}^{3}(\square^{1})$ are both sent to the degenerate 1-cube whose faces are the rightmost 0-cube of $\mathsf{Sd}_{\leq 1}^{2}(\square^{1})$.

By the 2-universal property of $\square$, the natural transformation 

<div style="text-align: center;">

<img src="https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_16-figure27.png" alt="" />

</div>

gives rise to a natural transformation as follows.

<div style="text-align: center;">

<img src="https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_16-figure28.png" alt="" />

</div>

By means of the 2-universal property of $\mathsf{Set}^{\square^{op}}$ as a free co-completion, this natural transformation $\phi^{n+1}$ gives rise to a natural transformation as follows.

<div style="text-align: center;">

<img src="https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_16-figure29.png" alt="" />

</div>
