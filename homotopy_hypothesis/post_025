18/02/2015
Author: Richard Williamson
Format: MarkdownItex
Thank you for the encouragement, Karol! 

Thank you very much to you both for the interesting comments. Please jump in with references as you have done here whenever you wish!

I was aware of both the paper of Latch, Thomason and Wilson and the work of Cisinski, but had forgotten about these proofs! It is great that you have reminded me. 

> I think that this approach should work over an elegant Reedy category (is $\square$ such?)

I had not thought much about this before, but, yes, it seems to me that $\square$ is an elegant Reedy category. That it is a Reedy category is well known: a reference for the third property in the definition of a [Reedy category](http://ncatlab.org/nlab/show/Reedy+category) given in the nLab is Lemma 4.1 in the paper [_Cubical sets and their site_](http://tac.mta.ca/tac/volumes/11/8/11-08abs.html) of Grandis and Mauri, but the result for example also appears as Lemme 8.4.4 in the work [_Les préfaisceaux comme modèles des types d’homotopie_](http://www.math.univ-toulouse.fr/~dcisinsk/ast.pdf) of Cisinski, and probably has been known for much longer. Given the usual presentation of $\square$ by generators and relations, it is straightforward to see how a proof should go, appealing to the functoriality of $- \otimes -$, although writing down a truly rigorous proof might take some care. 

I have not found a reference which observes that $\square$ is an _elegant_ Reedy category, but I think this should follow from Proposition 1.2.1 in [_Notes on simplicial homotopy theory_](http://mat.uab.cat/~kock/crm/hocat/advanced-course/Quadern47.pdf) by Joyal and Tierney, which is one way to prove it for $\Delta$. I haven't checked all the details carefully, though.

If I read Theorem 5.1 in the paper of Grandis and Mauri correctly, it would seem to me that the category of cubes with connections can also be viewed as a Reedy category, if we regard both co-degeneracies and co-connections as belonging to $R_{-}$. However, this doesn't seem particularly natural to me. The result of Grandis and Mauri would rather suggest considering factorisations into three kinds of maps, not only two. I've not thought about whether the category of cubes with connections, if able to be viewed as a Reedy category in this way, is elegant, but it seems possible.

>I think that this approach should work over an elegant Reedy category provided that a reasonable Ex functor can be constructed

That would be interesting! More generally, it would be very interesting to see any kind of serious work along these lines in abstract homotopy theory, namely working in an axiomatic framework that captures both simplicial and cubical sets. Of course there is the notion of a test category, but this seems to me to be too general. The notion of a Reedy category is closer, but still does not feel right to me. One reason is given above, but, more conceptually, it doesn't seem to me the existence/uniqueness of the factorisations required in the definition of a Reedy category is at the heart of why $\Delta$ and $\square$ are good categories of shapes for abstract homotopy theory. I have never used this when working with cubical sets, for instance.

My feeling is that the category of cubes is somehow 'universal' amongst categories of shapes for abstract homotopy theory. It would be interesting, taking this idea seriously, to try to formally develop the homotopy theory of simplicial sets 'from a cubical point of view'. A point of view that is a little similar to this can be found in Jardine's two papers on cubical homotopy theory: [_Cubical homotopy theory - a beginning_](http://hopf.math.purdue.edu/Jardine/cubical2.ps.gz) and [_Categorical homotopy theory_](http://www.math.uiuc.edu/K-theory/0669/). The question of formalising the notion of a 'decorated category of cubes' that I mentioned in #9 is closely related.

The proof that I will give is of a very different flavour from that of Latch, Thomason, and Wilson, and that of Cisinski. Perhaps one could summarise the difference by saying that the proof I will give is 'structural' rather than 'combinatorial'. But this will become clearer when I've given further details.
