18/02/2015
Author: Karol Szumiło
Format: MarkdownItex
> My feeling is that the category of cubes is somehow ’universal’ amongst categories of shapes for abstract homotopy theory.

Personally, I feel that this distinction should go to $\Delta$ but that's just based on my own astonishment at how neat and elegant simplicial homotopy theory is. If I try to think about it in an unbiased way I don't see any clear advantages of either approach. In both cases we start with some basic notion of a $1$-dimensional homotopy and we follow some pattern of generating notions of homotopies of higher dimensions, both look equally ad hoc to me. I see where you are coming from with the universal property of $\square$ but the choice of a monoidal category as the basic structure seems just as arbitrary as the choice of a Reedy category.

> That would be interesting! More generally, it would be very interesting to see any kind of serious work along these lines in abstract homotopy theory, namely working in an axiomatic framework that captures both simplicial and cubical sets.

In one of his papers Clemens Berger mentions a notion of a "geometric Reedy category" that he tried to develop together with Cisinski that was supposed to do something like that. I asked him about it and from what he told me it seemed that the project didn't get too far and that their ideas weren't particularly compatible with the subdivision/$\Ex$ approach. This is a problem that I occasionally ponder idly but somehow it never takes off...
