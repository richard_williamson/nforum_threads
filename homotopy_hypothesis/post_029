20/02/2015
Author: Richard Williamson
Format: MarkdownItex
> Again, the choice of the monoidal structure of Top as the starting point is arbitrary.

Yes, I would agree with that. When looking for an axiomatic framework, one of course has to start somewhere, in a way which is going to be a matter of taste and efficacy (whether it will lead to a good theory). I personally find developing homotopy theory in the setting of structured intervals/cylinders/co-cylinders together with a monoidal structure very satisfactory, but certainly what I wrote about finding the monoidal structure of the category of cubes more natural than its Reedy structure is a consequence of, or at least very closely tied to, this point of view. I agree entirely that one may well be able to make a convincing argument for a different point of view. 

Maybe a better illustration of the difference I see between the monoidal and Reedy settings is that everything in the monoidal setting is built up from a handful of objects and arrows that are part of the structure, whereas in the Reedy setting one needs to know something about all arrows as part of the structure (this is also the case with the cylindrical approach to abstract homotopy theory of Kamps, which relies upon 'Kan-like conditions'). But I completely agree that there will not be an absolute reason why the one should be considered preferable over the other: one will only be able to give justifications that take certain things as common ground.

> I’m looking forward to the continuation of your write up.

Thank you! My apologies for the delay, a few things cropped up, but I have nearly finished preparing the next posts now. I'm not quite sure when I'll get the chance to put the finishing touches, but at the latest it should be early next week.
