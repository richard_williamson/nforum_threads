09/03/2015
Author: Richard Williamson
Format: MarkdownItex
There is a different possibility for getting around the lack of strictness of identities. Instead of working with $\mathsf{Ex}^{\infty}$, we could work with the _homotopy_ colimit of the diagram used to define $\mathsf{Ex}^{\infty}$, namely the homotopy colimit of the following diagram.

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure24.png' alt='' />

</div>

By 'homotopy colimit' here, I mean that we take the (ordinary!) colimit of the modification of the diagram above in which we 'insert a cylinder' at every step: we replace the natural transformation 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure25.png' alt='' />

</div>

by the following diagram of natural transformations

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure26.png' alt='' />

</div>

and replace the natural transformation

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure27.png' alt='' />

</div>

for every $n \geq 1$ by the following diagram of natural transformations.

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure28.png' alt='' />

</div>

Let us denote the homotopy colimit, in this sense, of the diagram

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure29.png' alt='' />

</div>

by $\mathsf{Ex}^{\infty}_{h}$.

Assuming that $\underline{\mathsf{I}}$ is equipped with what Grandis terms a 'zero collapse structure' in the paper [Categorically algebraic foundations for homotopical algebra](http://www.dima.unige.it/~grandis/HX1.pdf), which gives a means to construct a homotopy between $c_{x_{1}} \cdot f$ and $f$ for a 1-cube 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure30.png' alt='' />

</div>

of $X$, we will be able to construct a map from $\mathsf{Ex}_{h}^{\infty}(X)$ to $S\big( \left| X \right| \big)$. 

For every cubical set $X$, I believe it possible to demonstrate that 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure31.png' alt='' />

</div>

is a cofibration, in the sense of VIII of my thesis, if $\underline{\mathsf{I}}$ is equipped with a certain structure, and, given this same structure, that 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure32.png' alt='' />

</div>

is a cofibration. 

It should then follow that 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure33.png' alt='' />

</div>

is a homotopy equivalence for any cubical set $X$, where 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure34.png' alt='' />

</div>

is the canonical morphism of cubical sets.

However, I will not follow this approach. Instead, I follow the simpler 'tweaking' method that I described first: I wish to give as 'down to earth' a proof as possible, which can hopefully give people a clear understanding of why we should expect 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure35.png' alt='' />

</div>

to be a homotopy equivalence for any cubical set $X$.
