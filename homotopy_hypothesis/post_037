09/03/2015
Author: Richard Williamson
Format: MarkdownItex
Let us now work towards defining $\rho_{1}$. I will for the time being use the original notation $\mathsf{Ex}$ and $\mathsf{Sd}$ instead of $\mathsf{Ex}_{1}$ and $\mathsf{Sd}_{1}$, and the same in similar cases. When we come to work towards defining $\rho_{n}$ for $n \geq 2$, we'll return to the notation $\mathsf{Ex}_{n}$ and $\mathsf{Sd}_{n}$.  

Let us denote by 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure46.png' alt='' />

</div>

the canonical arrow of $\mathcal{A}$ such that the following diagram in $\mathcal{A}$ commutes.

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure47.png' alt='' />

</div>

Let us denote by 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure48.png' alt='' />

</div>

the canonical arrow of $\mathcal{A}$ such that the following diagram in $\mathcal{A}$ commutes.

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure49.png' alt='' />

</div>

We define a natural transformation 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure50.png' alt='' />

</div>

in the following way.

1) To the object $I^{0}$, we associate the following identity arrow.

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure51.png' alt='' />

</div>

2) To the object $I^{1}$, we associate the arrow 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure52.png' alt='' />

</div>

of $\mathcal{A}$.

It is straightforward to check that the three diagrams that must commute do indeed commute (for the diagram involving $p$, we appeal to both our assumption that the subdivision structure $(\mathsf{S},r_{0}^{\mathcal{A}},r_{1}^{\mathcal{A}},s^{\mathcal{A}})$ is compatible with $p^{\mathcal{A}}$ and that the involution structure $v^{\mathcal{A}}$ is compatible with $p^{\mathcal{A}}$).

Noting that it is the functor 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure53.png' alt='' />

</div>

to which the functor $\left| \mathsf{Sd}_{\leq 1}(-) \right|$ gives rise by means of the universal property of $\square$, the natural transformation $\tau_{1}^{\leq 1}$ gives rise, by means of the 2-universal property of $\square$, to a natural transformation as follows.

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure54.png' alt='' />

</div>

Noting that it is the functor 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure55.png' alt='' />

</div>

to which the functor 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure56.png' alt='' />

</div>

gives rise by means of the universal property of free co-completion, the natural transformation $\tau_{1}^{\square}$ gives rise, by means of the 2-universal property of free co-completion, to a natural transformation as follows.

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure57.png' alt='' />

</div>
