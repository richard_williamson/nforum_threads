09/03/2015
Author: Richard Williamson
Format: MarkdownItex
14) In addition, the following diagram commutes.

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure89.png' alt='' />

</div>

15) We conclude from 13) and 14) that the following diagram commutes.

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure90.png' alt='' />

</div>

16) Noting that it is the natural transformation 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure91.png' alt='' />

</div>

to which the natural transformation

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure92.png' alt='' />

</div>

gives rise by means of the 2-universal property of $\square$ and the 2-universal property of free co-completion, we conclude from 15) that the following diagram commutes, as we wished to show.

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure93.png' alt='' />

</div>

The remainder of our proof that the diagram 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure94.png' alt='' />

</div>

commutes will be a reduction to the diagram

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure95.png' alt='' />

</div>

whose commutativity we have just established.

To carry out this reduction, we will need a few observations which hold in an arbitrary strict 2-category $\mathcal{C}$. 

1) Let 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure96.png' alt='' />

</div>

be a 2-arrow of $\mathcal{C}$, and let

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure97.png' alt='' />

</div>

be a 2-arrow of $\mathcal{C}$. Then the following diagram commutes. 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure98.png' alt='' />

</div>

Let us demonstrate this. By definition, $\zeta_{1} \circ \zeta_{0}$ is obtained by pasting the 2-arrows in the following diagram. I have omitted the label of the 1-arrow in the middle, which is the identity. 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure99.png' alt='' />

</div> 

Since 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure100.png' alt='' />

</div>

is an identity for horizontal composition of 2-arrows, we deduce that $\zeta_{1} \circ \zeta_{0}$ is obtained by pasting the 2-arrows in the following diagram. Both of the 1-arrows in the middle are the identity. 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure101.png' alt='' />

</div>

Since 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure102.png' alt='' />

</div>

is by definition an identity for vertical composition of 2-arrows, we deduce that $\zeta_{1} \circ \zeta_{0}$ is obtained by pasting the 2-arrows in the following diagram.

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure103.png' alt='' />

</div> 

Appealing to the fact that 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure104.png' alt='' />

</div>
  
is by definition an identity for vertical composition, and similarly that 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure105.png' alt='' />

</div>

is by definition an identity for vertical composition, we deduce that $\zeta_{1} \circ \zeta_{0}$ is obtained by pasting the 2-arrows in the following diagram. The leftmost of the two 1-arrows in the middle is $f_{1}$, and the rightmost is $f_{0}$. 

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure106.png' alt='' />

</div>

Carrying out the pasting of these 2-arrows first horizontally and then vertically, we deduce that $\zeta_{1} \circ \zeta_{0}$ is obtained by pasting the 2-arrows in the following diagram, as we wished to show.

<div style='text-align: center;'>

<img src='https://bitbucket.org/richard_williamson/diagrams/raw/master/homotopy_hypothesis/post_32-figure107.png' alt='' />

</div>
